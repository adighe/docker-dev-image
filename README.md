Web development container configured to run a Centos7 server with PHP 7 and Apache installed
=======================

Versions
===========
0.1 - initial

0.2 - changed apache conf to fix broken css

Setting Up MySQL (If they are not set up)
==================

docker run --name=mariadb-data --restart=always -v /var/lib/mysql centos/mariadb true

docker run --name=mariadb --restart=always -d -p 33067:3306 --volumes-from=mariadb-data -e MYSQL_ROOT_PASSWORD=root centos/mariadb


Running Containers Instructions
===============================

docker run -p 9000:80 --restart=always -d -v /localpath:/var/www/html adighe/dev


Building Image (Only if you want to change it)
==============
docker build -t adighe/dev .





