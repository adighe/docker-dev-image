FROM centos:centos7
MAINTAINER Adrian Gheorghe <adrian.gheorghe02@gmail.com>

RUN yum -y install --setopt=tsflags=nodocs epel-release && \
    yum -y install --setopt=tsflags=nodocs mariadb-server bind-utils pwgen psmisc hostname && \
    yum -y update && yum clean all


# Install epel && wget && net-tools
RUN \
    yum -y install epel-release && \
    rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7 && \
    yum -y update && \
    yum -y install wget net-tools httpd hostname pwgen mc


# Install PHP
# Add repo
RUN wget http://rpms.remirepo.net/enterprise/remi-release-7.rpm && \
    rpm -Uvh remi-release-7.rpm && \
    yum-config-manager --enable remi-php70 && \
    rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-remi && \
    yum -y update && \
    yum -y install php php-mysql php-gd openssl psmisc tar git which zip unzip php-xml php-mbstring php-pecl-zendopcache drush sendmail


# Install Composer.
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer

# Install Symfony
#RUN curl -LsS https://symfony.com/installer -o /usr/local/bin/symfony && \
#    chmod a+x /usr/local/bin/symfony

#RUN php -r "readfile('https://s3.amazonaws.com/files.drush.org/drush.phar');" > drush
#RUN chmod +x drush
#RUN mv drush /usr/local/bin
#RUN drush init

RUN composer global require drupal/console:@stable

RUN cd /var/www && \
    chmod -R 777 /var/www && \
    rm -rf html && \
    mkdir html

# EXPOSE PORTS
EXPOSE 80 22 443

# Create Volume for the file
VOLUME /var/www/html

COPY start.sh /start.sh
RUN chmod -v +x /start.sh

COPY httpd.conf /etc/httpd/conf/httpd.conf

CMD ["/start.sh"]



